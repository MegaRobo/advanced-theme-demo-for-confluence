# README #

The purpose of the Advanced Theme Demo is to demonstrate how you
can create custom themes for Refined**Theme** for Confluence.

You can develop custom themes with your favourite IDE or with the online
advanced theme editor included in Refined**Theme** for Confluence.

Please note that you need to run Refined**Theme** for Confluence 5.0 or later.

Do you want to use SASS? Check out this branch https://bitbucket.org/refinedwikiteam/advanced-theme-demo-for-confluence/src/sass

Check out the file *js/script.js* for a demo on how to use the [RefinedWiki JavaScript events](https://docs.refinedwiki.com/x/0wWCAg)

### Develop with IDE: ###

1. Open this project in your editor.
2. Make sure the settings in theme-uploader.sh/theme-uploader.cmd are valid.
3. Make changes to your theme.
4. Make sure that you have enabled external theme import. Navigate in Confluence to: Config > Theme Configurations > Themes > Advanced Themes and click on "Enable".
5. Update the theme-uploader.sh/theme-uploader.cmd with correct information. 
6. Automatically upload your theme by running ./theme-uploader.sh (mac & linux, make sure it's executable run: "chmod +x theme-uploader.sh") or ./theme-uploader.cmd (Windows)
7. See your uploaded theme in confluence. Navigate in Confluence to: Config > Theme Configurations > Themes > Advanced Themes


### Develop with online code editor ###

1. In Confluence, go to: Config > Theme Configuration > Themes > Advanced Themes
2. Upload the Advanced Theme Demo(demotheme.zip) through the import advanced theme feature.
3. Hover the "Cog" and click edit for your theme.
4. Start coding..